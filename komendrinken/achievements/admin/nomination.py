from django.contrib import admin, messages
from achievements.models import Nomination

class NominationAdmin(admin.ModelAdmin):
    fieldsets = (
        ('',        {'fields': ('game', 'competitor', ('participated', 'won'), 'extra_votes',)}),
    )
    
    list_display    = ['game', 'competitor', 'participated', 'won', 'get_votes',]
    list_filter     = ['participated', 'won', 'extra_votes', 'game',]
    search_fields   = ['competitor__name', 'game__achievement__name', 'game__achievement__title', 'game__achievement__hint',]
    
    actions = ['undo_participation', 'undo_victory']
    
    # Actions
    def undo_participation(self, request, queryset):
        queryset.update(participated=False, won=False)
        msg = "Successfully undone %s participations."
        self.message_user(request, msg % str(len(queryset.filter(participated=True))), messages.SUCCESS)
        return
    
    def undo_victory(self, request, queryset):
        queryset.update(won=False)
        msg = "Successfully undone %s victories."
        self.message_user(request, msg % str(len(queryset.filter(won=True))), messages.SUCCESS)
        return
    
admin.site.register(Nomination, NominationAdmin)

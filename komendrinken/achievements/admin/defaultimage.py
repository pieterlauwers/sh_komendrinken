from django.contrib import admin
from achievements.models import DefaultImage

class DefaultImageAdmin(admin.ModelAdmin):
    fieldsets = (
        ('',        {'fields': (('photo', 'photo_thumb'), 'use')}),
    )

    list_display    = ['pk', 'photo', 'use',]
    list_filter     = ['use',]

admin.site.register(DefaultImage, DefaultImageAdmin)

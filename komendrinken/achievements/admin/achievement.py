from django.contrib import admin
from achievements.models import Achievement

class AchievementAdmin(admin.ModelAdmin):
    fieldsets = (
        ('',        {'fields': ('name', 'hint', 'title', 'players', 'description', 'explanation',)}),
    )
    
    list_display    = ['name', 'hint', 'title', 'players',]
    list_filter     = ['players',]
    search_fields   = ['name', 'hint', 'title', 'players', 'description', 'explanation',]
    
admin.site.register(Achievement, AchievementAdmin)

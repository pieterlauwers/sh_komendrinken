from django.contrib import admin
from achievements.models import Group

class GroupAdmin(admin.ModelAdmin):
    fieldsets = (
        ('',        {'fields': ('name',)}),
    )
    
    list_display    = ['name',]
    search_fields   = ['name',]
admin.site.register(Group, GroupAdmin)

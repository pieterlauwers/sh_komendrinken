from django.contrib import admin
from achievements.models import Vote

class VoteAdmin(admin.ModelAdmin):
    fieldsets = (
        ('',        {'fields': ('nomination', 'competitor',)}),
    )
    
    list_display    = ['nomination', 'competitor',]
    list_filter     = ['competitor',]
    search_fields   = ['competitor__name', 'nomination__game__achievement__name', 'nomination__game__achievement__title', 'nomination__game__achievement__hint',]
admin.site.register(Vote, VoteAdmin)

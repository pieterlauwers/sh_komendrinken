from django.contrib import admin
from achievements.models import Competitor

class CompetitorAdmin(admin.ModelAdmin):
    fieldsets = (
        ('',        {'fields': ('name', 'group', ('photo', 'photo_thumb'), ('excluded', 'blocked'), 'ip_source')}),
    )
    
    list_display    = ['name', 'group', 'excluded', 'blocked',]
    list_filter     = ['group', 'excluded', 'blocked',]
    search_fields   = ['name', 'group__name', 'ip_source']
admin.site.register(Competitor, CompetitorAdmin)

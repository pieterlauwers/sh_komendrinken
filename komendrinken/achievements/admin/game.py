from django.contrib import admin
from achievements.models import Game

class GameAdmin(admin.ModelAdmin):
    fieldsets = (
        ('',        {'fields': ('achievement', 'status', 'start_vote', 'end_vote',)}),
    )
    
    list_display    = ['achievement', 'pk', 'status', 'start_vote', 'end_vote',]
    list_filter     = ['status',]
    search_fields   = ['achievement__name', 'achievement__title', 'achievement__hint',]
admin.site.register(Game, GameAdmin)

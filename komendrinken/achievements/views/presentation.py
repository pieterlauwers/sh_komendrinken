from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from datetime import *
from django.db.models import Q
from achievements.models import *
from achievements.util import *
import sys

@login_required
def presentation(request):
    game = get_current_game()
    
    if game and game.status == "1":
        grouplist = []
        groups = Group.objects.all()
    
        for group in groups:
            grouplist.append((group, Nomination.objects.filter(game=game.pk, competitor__group=group.pk)))
    
        context = {
            "game"              : game,
            "grouplist"         : grouplist,
        }
        return render(request, "achievements/presentation/voting.html", context)
    
    if game and game.status == "2":
        context = {
            "game"              : game,
        }
        return render(request, "achievements/presentation/game.html", context)
    
    game = get_last_game()
    if game and game.status == "3":
        context = {
            "game"              : game,
            "competitors"       : Competitor.objects.all(),
            "groups"            : Group.objects.all(),
        }
        return render(request, "achievements/presentation/winners.html", context)
    
    grouplist = []
    groups = Group.objects.all()
    
    for group in groups:
        grouplist.append((group, Competitor.objects.filter(group=group.pk, excluded=False)))

    context = {
        "grouplist"        : grouplist,
        "groups"           : groups,
        "game"             : game,
    }
    
    return render(request, "achievements/presentation/competitors.html", context)
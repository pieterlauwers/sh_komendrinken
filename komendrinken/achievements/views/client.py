from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from datetime import *
from django.db.models import Q
from achievements.models import *
from achievements.util import *
import sys

def get_competitor(request):
    competitor = Competitor.objects.filter(ip_source=getIP(request))
    if not competitor and get_active_competitor(request):
        competitor = get_active_competitor(request)
        competitor.ip_source = getIP(request)
        competitor.save()
    elif competitor:
        competitor = competitor[0]
        
    if competitor:
        set_active_competitor(request, competitor.pk)
        
    return competitor

def client(request):
    competitor = get_competitor(request)
    game = get_current_game()
    
    if not competitor:
        context = {
            "groups"            : Group.objects.all(),
        }

        return render(request, "achievements/client/signup.html", context)
    if game and game.status == "1" and not Vote.objects.filter(competitor=competitor.pk, nomination__game=game.pk):
        context = {
            "game"              : game,
            "competitor"        : competitor,
        }

        return render(request, "achievements/client/vote.html", context)
    else:
        if game:
            if game.status == "0":
                message = "Er staat een nieuw spel klaar, nog even geduld."
            elif game.status == "1":
                message = "Je hebt je stem voor het huidige spel al uitgebracht."
            elif game.status == "2":
                message = "De stemming voor het huidige spel is afgerond."
        else:
            message = "Er is momenteel geen stemming bezig."
        context = {
            "competitor"        : competitor,
            "message"           : message,
        }
        return render(request, "achievements/client/waiting.html", context)
    
def signUp(request):
    competitor = Competitor()
    error = None
    noForm = False
    admin = False
    
    try:
        competitor.name         = request.POST['name']
        if Competitor.objects.filter(name=request.POST['name']):
            error = "De naam die u heeft gekozen bestaat al, gelieve een andere te kiezen."
            
        competitor.group        = Group.objects.get(id=request.POST['group'])

        if "photo" in request.FILES:
            competitor.photo        = request.FILES["photo"]
                
        admin = request.POST['source'] == 'dashboard'
        
    except (KeyError):
        # Redisplay the form
        error = "Gelieve alle velden correct in te vullen!"

    if not admin:
        try:
            competitor.ip_source = getIP(request)
            if Competitor.objects.filter(ip_source=getIP(request)):
                error = "Dit apparaat staat reeds geregistreed. Klik <a href=\"/\">hier</a>."
                noForm = True
        except:
            error = "Probleem met het detecteren van het apparaat. Probeer later opnieuw"
            print "Unexpected error while parsing IP of source:", sys.exc_info()[0]
        
    if not error:
        try:
            competitor.save()
        except (IOError):
            error = "Gelieve een geldige foto te kiezen."
    
    if error:
        return render(request, "achievements/client/signup.html", {
            'competitor'    : competitor,
            'error_message' : error,
            'groups'        : Group.objects.all(),
            'noForm'        : noForm,
        })
    
    if admin:
        return HttpResponseRedirect(reverse('achievements:dashboard'))
    
    set_active_competitor(request, competitor.pk)
    
    # Always return an HttpResponseRedirect after successfully dealing
    # with POST data. This prevents data from being posted twice if a
    # user hits the Back button.
    return HttpResponseRedirect(reverse('achievements:home'))

def vote(request, game_id):
    game = get_current_game()

    if game and int(game.pk) == int(game_id):
        error = None
        admin = False
        
        try:
            admin = request.POST['source'] == 'dashboard'

        except (KeyError):
            admin = False
            
        try:
            if admin:
                competitor = Competitor.objects.get(pk=int(request.POST['competitor']))
            else:
                competitor = get_competitor(request)

            if competitor:
                if not Vote.objects.filter(competitor=competitor.pk, nomination__game=game.pk):

                    votes = []
                    for key in request.POST:
                        if key.startswith('player'):
                            nomination = Nomination.objects.get(pk=int(key.strip('player')))
                            votes += [Vote(nomination=nomination, competitor=competitor)]

                    if not votes:
                        error = "Selecteer 3 personen om je stem uit te brengen."
                    if len(votes) > 3:
                        error = "Je stem is ongeldig. Selecteer maximum 3 personen."
                        
                else:
                    error = "U heeft reeds een stem uitgebracht."
            else:
                error = "Kan de gebruiker niet detecteren."

        except (KeyError):
            error = "Gelieve alle velden correct in te vullen!"

        if error:
            return render(request, "achievements/client/vote.html", {
                "game"              : get_current_game(),
                "competitor"        : get_competitor(request),
                'error_message'     : error,
            })
        
        for vote in votes:
            vote.save()

        if admin:
            return HttpResponseRedirect(reverse('achievements:dashboard'))
        
        
    return HttpResponseRedirect(reverse('achievements:home'))
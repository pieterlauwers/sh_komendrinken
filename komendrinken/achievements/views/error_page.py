from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.utils.translation import ugettext as _

def page_not_found(request):
    error = '404'
    tekst = 'This page was not found.'
    context = {
        "error"                 : error,
        "error_text"            : tekst,
    }
    return render(request, "achievements/error_page.html", context)

def server_error(request):
    error = '500'
    tekst = 'Internal server error.'
    ticket = Ticket.objects.latest("date_created")
    context = {
        "error"                 : error,
        "error_text"            : tekst,
        "ticket"                : ticket.pk,
    }
    return render(request, "achievements/error_page.html", context)

def permission_denied(request):
    error = '403'
    tekst = 'Forbidden error.'
    context = {
        "error"                 : error,
        "error_text"            : tekst,
    }
    return render(request, "achievements/error_page.html", context)

def bad_request(request):
    error = '400'
    tekst = 'Bad request error.'
    context = {
        "error"                 : error,
        "error_text"            : tekst,
    }
    return render(request, "achievements/error_page.html", context)
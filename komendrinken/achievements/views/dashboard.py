from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from datetime import *
from django.db.models import Q
from achievements.models import *
from achievements.util import *
import sys

@login_required
def dashboard(request):
    grouplist = []
    groups = Group.objects.all()
    
    for group in groups:
        grouplist.append((group, Competitor.objects.filter(group=group.pk)))
        
    game = get_current_game()

    context = {
        "grouplist"        : grouplist,
        "groups"           : groups,
        "achievements"     : Achievement.objects.all(),
        "game"             : game,
    }
    
    if game:
        if game.status == '0':
            return render(request, "achievements/dashboard/schedule_game.html", context)
        if game.status == '1':
            context["competitors"] = Competitor.objects.all().order_by("name")
            return render(request, "achievements/dashboard/voting.html", context)
        if game.status == '2':
            return render(request, "achievements/dashboard/winners.html", context)
        
    return render(request, "achievements/dashboard/new_game.html", context)

@login_required
def newGame(request, achievement_id):
    achievement = get_object_or_404(Achievement, pk=achievement_id)
    game = Game(achievement=achievement)
    game.save()
    return HttpResponseRedirect(reverse('achievements:dashboard'))

@login_required
def startVoting(request, game_id):
    game = get_object_or_404(Game, pk=game_id)
    game.start_voting()
    return HttpResponseRedirect(reverse('achievements:dashboard'))

@login_required
def scheduleVoting(request, game_id):
    game = get_object_or_404(Game, pk=game_id)
        
    try:
        start_vote = 0
        if 'start_vote' in request.POST and request.POST['start_vote']:
            offset = int(request.POST['start_vote'])
            start_vote = offset
            game.start_vote = datetime.now() + timedelta(0, offset * 60)
        if 'end_vote' in request.POST and request.POST['end_vote']:
            offset = int(request.POST['end_vote'])
            game.end_vote = datetime.now() + timedelta(0, (offset + start_vote) * 60)

        game.save()
        
    except:
        print "ERROR: something went wrong while scheduling start of the voting"
        
    return HttpResponseRedirect(reverse('achievements:dashboard'))

@login_required
def startGame(request, game_id):
    game = get_object_or_404(Game, pk=game_id)
    game.start_game()
    return HttpResponseRedirect(reverse('achievements:dashboard'))

@login_required
def endGame(request, game_id):
    game = get_object_or_404(Game, pk=game_id)
    
    winners = []
    for key in request.POST:
        if key.startswith('player'):
            winners += [int(key.strip('player'))]
            
    game.end_game(winners)
    return HttpResponseRedirect(reverse('achievements:dashboard'))  
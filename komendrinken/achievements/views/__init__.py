from dashboard import dashboard, newGame, startVoting, scheduleVoting, startGame, endGame
from client import client, signUp, vote
from presentation import presentation
from error_page import page_not_found, server_error, permission_denied, bad_request
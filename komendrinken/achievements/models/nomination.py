from django.db import models
from vote import Vote

class Nomination(models.Model):
    class Meta:
        app_label               = 'achievements'
        
    game            = models.ForeignKey('Game')
    competitor      = models.ForeignKey('Competitor')
    participated    = models.BooleanField(default=False)
    won             = models.BooleanField(default=False)
    extra_votes     = models.IntegerField(default=0)
    
    def __unicode__(self):
        """
        Returns a string representation of this object.

        @return string, string representation
        """
        return str(self.game) + ": " + str(self.competitor)
    
    def get_votes(self):
        return Vote.objects.filter(nomination=self.pk).count() + self.extra_votes
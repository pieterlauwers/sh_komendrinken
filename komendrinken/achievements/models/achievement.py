from django.db import models
from game import Game

class Achievement(models.Model):
    class Meta:
        app_label               = 'achievements'
    
    name        = models.CharField(max_length=30, unique=True)
    hint        = models.CharField(max_length=30)
    title       = models.CharField(max_length=30)
    players     = models.PositiveIntegerField()
    description = models.TextField(blank=True)
    explanation = models.TextField(blank=True)
    
    def __unicode__(self):
        """
        Returns a string representation of this object.

        @return string, string representation
        """
        return self.name
    
    def get_games(self):
        return Game.objects.filter(achievement=self.pk).count()
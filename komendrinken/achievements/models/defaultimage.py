from django.db import models

# Imports for image handling
import StringIO
from PIL import Image, ExifTags
from django.core.files.base import ContentFile

class DefaultImage(models.Model):
    class Meta:
        app_label               = 'achievements'
    
    photo       = models.ImageField(blank=True)
    photo_thumb = models.ImageField(blank=True)
    use         = models.PositiveIntegerField(default=0)
    
    def save(self, *args, **kwargs):   
        if self.photo and not self.photo_thumb:
            image = Image.open(self.photo.file)
            
            # Rotate image depending on meta information about the orientation
            try:
                if image._getexif():
                    for orientation in ExifTags.TAGS.keys() : 
                        if ExifTags.TAGS[orientation]=='Orientation' : break 
                    exif=dict(image._getexif().items())

                    if   exif[orientation] == 3 : 
                        image=image.rotate(180, expand=True)
                    elif exif[orientation] == 6 : 
                        image=image.rotate(270, expand=True)
                    elif exif[orientation] == 8 : 
                        image=image.rotate(90, expand=True)
            except:
                print "ERROR: couldn't fetch Meta tags from the picture"

            w, h = image.size
            size = 512
            print w, h 
            if w == h:
                w, h = size, size
            elif w > h:
                w = int(w * (float(size)/h))
                h = size
            else:
                h = int(h * (float(size)/w))
                w = size
            print w, h 
            
            image = image.resize((w, h), Image.ANTIALIAS)
            print ((w-size)/2, (h-size)/2, size + (w-size)/2, size + (h-size)/2)
            image = image.transform((size, size), Image.EXTENT, ((w-size)/2, (h-size)/2, size + (w-size)/2, size + (h-size)/2))
            
            image_file_name = str(self.photo.file).replace('.', '_thumb.')
            image_file = ContentFile(image_file_name)
            image_file_name = image_file_name.split('/')[-1]
            image.save(image_file, 'JPEG', quality=90)
            self.photo_thumb.save(image_file_name, image_file, save=False)
            
        super(DefaultImage, self).save(*args, **kwargs)
            
from django.db import models
from competitor import Competitor

class Group(models.Model):
    class Meta:
        app_label               = 'achievements'
        ordering                = ['pk']
    
    name        = models.CharField(max_length=30)
    
    def __unicode__(self):
        """
        Returns a string representation of this object.

        @return string, string representation
        """
        return self.name
    
    def get_victories(self):
        victories = 0
        members = Competitor.objects.filter(group=self.pk)
        for member in members:
            victories += member.get_victories()
        
        return victories
        
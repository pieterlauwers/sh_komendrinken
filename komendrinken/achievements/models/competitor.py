from django.db import models
from defaultimage import DefaultImage
from nomination import Nomination

# Imports for image handling
import StringIO
from PIL import Image, ExifTags
from django.core.files.base import ContentFile

class Competitor(models.Model):
    class Meta:
        app_label               = 'achievements'
    
    name        = models.CharField(max_length=30, unique=True)
    photo       = models.ImageField(blank=True)
    photo_thumb = models.ImageField(blank=True)
    group       = models.ForeignKey('Group')
    excluded    = models.BooleanField(default=False)
    blocked     = models.BooleanField(default=False)
    ip_source   = models.GenericIPAddressField(blank=True, null=True)
    
    def __unicode__(self):
        """
        Returns a string representation of this object.

        @return string, string representation
        """
        return self.name
    
    def get_nominations(self):
        return Nomination.objects.filter(competitor=self.pk).count()
    
    def get_played_games(self):
        return Nomination.objects.filter(competitor=self.pk, participated=True).count()
    
    def get_victories(self):
        return Nomination.objects.filter(competitor=self.pk, won=True).count()
    
    def get_titles(self):
        titles = []
        victories = Nomination.objects.filter(competitor=self.pk, won=True)
        for victory in victories:
            titles += [victory.game.achievement.title]
            
        return titles
    
    def save(self, *args, **kwargs):   
        if self.photo and not self.photo_thumb:
            image = Image.open(self.photo.file)
            
            # Rotate image depending on meta information about the orientation
            try:
                for orientation in ExifTags.TAGS.keys() : 
                    if ExifTags.TAGS[orientation]=='Orientation' : break 
                exif=dict(image._getexif().items())

                if   exif[orientation] == 3 : 
                    image=image.rotate(180, expand=True)
                elif exif[orientation] == 6 : 
                    image=image.rotate(270, expand=True)
                elif exif[orientation] == 8 : 
                    image=image.rotate(90, expand=True)
            except:
                print "ERROR: couldn't fetch Meta tags from the picture"

            try:
                w, h = image.size
                size = 512
                print w, h 
                if w == h:
                    w, h = size, size
                elif w > h:
                    w = int(w * (float(size)/h))
                    h = size
                else:
                    h = int(h * (float(size)/w))
                    w = size
                print w, h 

                image = image.resize((w, h), Image.ANTIALIAS)
                print ((w-size)/2, (h-size)/2, size + (w-size)/2, size + (h-size)/2)
                image = image.transform((size, size), Image.EXTENT, ((w-size)/2, (h-size)/2, size + (w-size)/2, size + (h-size)/2))

                image_file_name = str(self.photo.file).replace('.', '_thumb.')
                image_file = ContentFile(image_file_name)
                image_file_name = image_file_name.split('/')[-1]
                image.save(image_file, 'JPEG', quality=90)
                self.photo_thumb.save(image_file_name, image_file, save=False)
            except:
                print "ERROR: couldn't resize image, using a default"
                self.photo = None
        
        if not self.photo:
            image = DefaultImage.objects.all().order_by('use')[0]
            self.photo = image.photo
            self.photo_thumb = image.photo_thumb
            image.use += 1
            image.save()
            
        super(Competitor, self).save(*args, **kwargs)
            
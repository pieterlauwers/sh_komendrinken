from django.db import models

class Vote(models.Model):
    class Meta:
        app_label               = 'achievements'
    
    nomination      = models.ForeignKey('Nomination')
    competitor      = models.ForeignKey('Competitor')
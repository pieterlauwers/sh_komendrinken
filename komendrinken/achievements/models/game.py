from django.db import models
from nomination import Nomination
from nomination import Vote
from competitor import Competitor
from random import shuffle
from datetime import datetime
from django.utils.timezone import utc

class Game(models.Model):
    class Meta:
        app_label               = 'achievements'
        
    status_choices = (
        ('0', 'waiting'),
        ('1', 'voting'),
        ('2', 'playing'),
        ('3', 'finished'),
    )
    
    achievement     = models.ForeignKey('Achievement')
    status          = models.CharField(max_length=1, choices=status_choices, default='0')
    start_vote      = models.DateTimeField(blank=True, null=True)
    end_vote        = models.DateTimeField(blank=True, null=True)
    
    def __unicode__(self):
        """
        Returns a string representation of this object.

        @return string, string representation
        """
        return str(self.achievement) + " - " + str(self.pk)
    
    def get_nominations(self):
        return Nomination.objects.filter(game=self.pk)
    
    def get_players(self):
        return Nomination.objects.filter(game=self.pk, participated=True)
    
    def get_winners(self):
        return Nomination.objects.filter(game=self.pk, won=True)
    
    def select_players(self):
        number_players = self.achievement.players
        nominations = list(Nomination.objects.filter(game=self.pk))
        shuffle(nominations)
        nominations = sorted(nominations, key=lambda a: a.get_votes())
        nominations = list(reversed(nominations))
        players = nominations[:number_players]
        
        for player in players:
            player.participated=True
            player.save()
        
    def nominate_competitors(self):
        competitors = list(Competitor.objects.filter(excluded=False))
        nominations = len(competitors) * 60 / 100
        shuffle(competitors)
        competitors = competitors[nominations:]
        for competitor in competitors:
            nomination = Nomination(game=self, competitor=competitor)
            nomination.save()
            
    def start_voting(self):
        if self.status != '0':
            print "ERROR: voting was already started"
            return False
        if Game.objects.filter(status='1'):
            print "ERROR: there is already a voting started for another game"
            return False
        if Game.objects.filter(status='2'):
            print "ERROR: there is an unfinished game"
            return False
        
        self.nominate_competitors()
        self.status = '1'
        self.start_voting = datetime.now()
        self.save()
    
    def start_game(self):
        if self.status != '1':
            print "ERROR: the game is not ready for start or has already been started"
            return False
        if Game.objects.filter(status='1').count() > 1:
            print "ERROR: there are multiple games in voting mode"
            return False
        if Game.objects.filter(status='2'):
            print "ERROR: there is another unfinished game"
            return False
        
        self.select_players()
        self.status = '2'
        self.end_voting = datetime.now()
        self.save()
        
    def end_game(self, winners):
        if self.status != '2':
            print "ERROR: the game hasn't been started yet or has already been ended"
            return False
        
        for winner in winners:
            nomination = Nomination.objects.get(pk=winner)
            nomination.won = True
            nomination.save()
            
        self.status = '3'
        self.save()
        
    def get_remaining_start(self):
        if not self.start_vote:
            return None
        now = datetime.utcnow().replace(tzinfo=utc)
        return (self.start_vote - now).total_seconds()
    
    def get_remaining_start_min(self):
        if not self.start_vote:
            return None
        return int(self.get_remaining_start() / 60)
    
    def get_remaining_end(self):
        if not self.end_vote:
            return None
        now = datetime.utcnow().replace(tzinfo=utc)
        return (self.end_vote - now).total_seconds()
    
    def get_remaining_end_min(self):
        if not self.end_vote:
            return None
        return int(self.get_remaining_end() / 60)
    
    def get_voting_progress(self):
        competitors = Competitor.objects.filter(blocked=False).count()
        print competitors
        print float(self.get_number_voters())
        print float(self.get_number_voters()) / competitors
        print int(float(self.get_number_voters()) / competitors * 100)
        return min(100, int(float(self.get_number_voters()) / competitors * 100))
    
    def get_number_voters(self):
        competitors = Competitor.objects.filter(blocked=False)
        votes = Vote.objects.filter(nomination__game=self)
        
        voters = 0
        for competitor in competitors:
            if votes.filter(competitor=competitor.pk).count():
                voters += 1

        return voters
    
    def get_number_votes(self):
        votes = Vote.objects.filter(nomination__game=self).count()
        return votes
    
    def check_timers(self):
        if self.get_remaining_start() and self.get_remaining_start() <= 0:
            self.start_voting()
        if self.get_remaining_end() and self.get_remaining_end() <= 0:
            self.start_game()
        
def get_current_game():
    games = Game.objects.all().exclude(status='3')
    if games:
        if games.count() > 1:
            print "ERROR: there are multiple active games"
        game = games[0]
        game.check_timers()
        return game

def get_last_game():
    if Game.objects.all():
        return Game.objects.all().order_by("-pk")[0]
    return None
from achievements.models import *

def getIP(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def get_active_competitor(request):
    if 'active_competitor' in request.session:
        pk = request.session['active_competitor']
        competitor = Competitor.objects.filter(pk=pk)
        if competitor:
            return competitor[0]
    return None

def set_active_competitor(request, pk):
    competitor = Competitor.objects.filter(pk=pk)
    if competitor:
        request.session['active_competitor'] = pk
        return True
    return False
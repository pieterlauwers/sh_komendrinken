from django.conf.urls import url

from achievements import views

urlpatterns = [
    url(r'^$', views.client, name="home"),
    url(r'^signup/$', views.signUp, name='handleSignUp'),
    url(r'^vote/(?P<game_id>\d+)/$', views.vote, name='handleVote'),
    
    url(r'^dashboard/$', views.dashboard, name='dashboard'),
    url(r'^game/new/(?P<achievement_id>\d+)$', views.newGame, name='newGame'),
    url(r'^game/voting/(?P<game_id>\d+)$', views.startVoting, name='startVoting'),
    url(r'^game/schedule/(?P<game_id>\d+)$', views.scheduleVoting, name='scheduleVoting'),
    url(r'^game/start/(?P<game_id>\d+)$', views.startGame, name='startGame'),
    url(r'^game/end/(?P<game_id>\d+)$', views.endGame, name='endGame'),
    
    url(r'^presentation/$', views.presentation, name='presentation'),
]
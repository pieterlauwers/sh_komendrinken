function countdown(remaining) {
    if(remaining <= 0)
        location.reload(true);
    var minutes = Math.floor(remaining / 60);
    var seconds = Math.floor(remaining % 60);
    document.getElementById('countdown').innerHTML = minutes.toString() + " minuten en " + seconds.toString() + " seconden";
    setTimeout(function(){ countdown(remaining - 1); }, 1000);
}

function refresh_rate(remaining) {
    if(remaining <= 0)
        location.reload(true);
    setTimeout(function(){ refresh_rate(remaining - 1); }, 1000);
}